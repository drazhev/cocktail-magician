import passport from "passport";
import { select, tables } from './data/database.js';

export const auth = passport.authenticate('jwt', { session: false });

export const isInRole = (...allowedRoles) => async (req, res, next) => {
    try {
        const [{ role }] = await select(`SELECT role FROM ${tables.users} WHERE id = ?`, [req.user.id]);

        if (allowedRoles.includes(role)) {
            next()
        } else {
            res.sendStatus(403);
        }
    } catch (e) {
        res.sendStatus(403);
    }
}

export const validator = schema => (req, res, next) => {
    const errors = Object.keys(schema)
        .map(key => schema[key](req.body[key]))
        .filter(e => e !== null);

    if (errors.length === 0) {
        next()
    } else {
        res.status(400).send(errors);
    }
};

export const handleIngredients = async (req, res, next) => {
    const ingredients = [...new Set(req.body.ingredients)];
    const availableIngredients = await select(
        `SELECT id, name from ${tables.ingredients} 
        WHERE name in (${ingredients.map(i => `'${i}'`).join(', ')})`);

    const mismatch = ingredients.length - availableIngredients.length;
    if (mismatch > 0) {
        return res.status(400).send({ message: `${mismatch} ingredients not available` });
    } else {
        req.customData = {
            ingredients: availableIngredients
        };
        next()
    }
};

export const barCocktailInterceptor = async (req, res, next) => {
    const { barId, cocktailId } = req.params;

    if ((await select(`SELECT id FROM ${tables.cocktails} where id = ?`, [cocktailId])).length === 0) {
        return res.status(404).send({ message: `Cocktail ${cocktailId} does not exist` });
    }

    if ((await select(`SELECT id FROM ${tables.bars} where id = ?`, [barId])).length === 0) {
        return res.status(404).send({ message: `Bar ${barId} does not exist` });
    }

    const [barCocktail] = await select(
        `SELECT cocktailId, barId
         FROM ${tables.barsCocktails} 
         WHERE cocktailId = ? AND barId = ?`,
        [cocktailId, barId]);

    req.customData = { barCocktail: barCocktail ?? null };
    next();
}

export const favouriteCocktailInterceptor = async (req, res, next) => {
    const results = await select(`SELECT id FROM ${tables.cocktails} where id = ?`, [req.params.cocktailId]);

    if (results.length === 0) {
        res.status(404).send({ message: `Cocktail ${req.params.cocktailId} does not exist` });
    } else {
        const [favourite] = await select(
            `SELECT cocktailId, userId
             FROM ${tables.favouriteCocktails} 
             WHERE cocktailId = ? AND userId = ?`,
            [req.params.cocktailId, req.user.id]);

        req.customData = { favourite: favourite ?? null };
        next();
    }
}