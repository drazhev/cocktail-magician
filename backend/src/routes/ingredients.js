import express from 'express';
import asyncHandler from 'express-async-handler';
import { insert, select, tables, update } from '../data/database.js';
import { roles } from '../data/roles.js';
import { auth, isInRole, validator } from '../middlewares.js';
import schema from '../validations.js';

const ingredients = express.Router();

ingredients.use(auth);
ingredients.use(isInRole(roles.MAGICIAN));

ingredients.get('/', asyncHandler(async (req, res) => {
    res.send(await select(`SELECT id, name, COUNT(c_i.ingredientId) as cocktailsCount 
                           FROM ${tables.ingredients} as i
                           LEFT JOIN ${tables.cocktailsIngredients} as c_i ON i.id = c_i.ingredientId
                           GROUP BY i.id`));
}));

ingredients.post('/', validator(schema.ingredient), asyncHandler(async (req, res) => {
    const [ingredient] = await select(
        `SELECT id, name FROM ${tables.ingredients} where name = ?`,
        [req.body.name]);

    if (ingredient) {
        return res.send(ingredient);
    }

    const { recordId } = await insert(
        `INSERT INTO ${tables.ingredients}(name) VALUES(?)`,
        [req.body.name]);

    res.send({ id: recordId, name: req.body.name });
}));

ingredients.delete('/:id', asyncHandler(async (req, res) => {
    const [{ count }] = await select(`SELECT COUNT(*) as count FROM ${tables.cocktailsIngredients} 
                                  WHERE ingredientId = ?;`, [req.params.id]);

    if (count > 0) {
        return res.status(409).send({ message: `Ingredient used in ${count} cocktails` });
    }

    const { rowsAffected } = await update(`DELETE FROM ${tables.ingredients} WHERE id = ?`, [req.params.id]);

    res.sendStatus(rowsAffected > 0 ? 200 : 404);
}));

export default ingredients;