import { Container, Grid } from "@mui/material";
import React, { useEffect, useState } from "react";
import CardViewCocktails from "../../views/CardViewCocktails/CardViewCocktails";
import nothingFound from '../../assets/nothing.png'
import "./Cocktails.css";

const Cocktails = () => {
  const [cocktails, setCocktails] = useState([]);
  const [showCocktails, setShowCocktails] = useState([]);
  const [searchWord, setSearchWord] = useState("");
  useEffect(() => {
    fetch("http://localhost:5000/cocktails/", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setCocktails(res);
        setShowCocktails(res);
      })
      .catch(({ message }) => {
        console.log(message);
      });
  }, []);

  return (
    <div className="cocktails" style={{ textAlign: "center" }}>
      <Container>
        <input
          className="search-for-cocktail"
          placeholder="Search for cocktail"
          text-align="center"
          type="text"
          value={searchWord}
          onChange={(e) => {
            setShowCocktails(
              cocktails.filter((c) =>
                c.name.toLowerCase().includes(e.target.value.toLowerCase())
              )
            );
            setSearchWord(e.target.value);
          }}
        />
        <Grid container spacing={2}>
          {showCocktails.length > 0 ? showCocktails.map((cocktail) => {
            return (
              <Grid item md={4} sm={6} mb={4} mt={4} key={cocktail.id}>
                <CardViewCocktails
                  className="cocktail-view"
                  cocktail={cocktail}
                />
              </Grid>
            );
          }) : 
          <Container style={{marginTop:'60px'}}>
            <img src={nothingFound} alt='Nothing found' />
          </Container>}
        </Grid>
      </Container>
    </div>
  );
};

export default Cocktails;
