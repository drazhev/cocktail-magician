import {
  Button,
  Modal,
  Table,
  TableCell,
  TableRow,
  Typography,
} from '@mui/material';
import { Box } from '@mui/system';
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router';
import AuthContext from '../../auth/AuthContext';

const style = {
  position: 'absolute',
  background: 'rgb(41, 50, 65)',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const Share = ({ open, setOpen, cocktail, load }) => {
  const handleClose = () => setOpen(false);
  const { user } = useContext(AuthContext);
  const history = useNavigate();

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          {load && cocktail.favouritedBy.length ? (
            cocktail.favouritedBy.map((u) =>
              user ? (
                u.displayName ? (
                  <Table>
                    <TableRow>
                      <TableCell>
                        <Button
                          sx={{
                            fontFamily: 'Apple Color Emoji',
                            color: 'orange',
                          }}
                          onClick={() =>
                            user.id === u.id
                              ? history('/profile')
                              : history(`/profile/${u.id}`)}
                          aria-label="singleView"
                        >
                          {u.displayName}
                        </Button>{' '}
                        <br />
                      </TableCell>
                    </TableRow>
                  </Table>
                ) : (
                  <Table>
                    <TableRow>
                      <TableCell>
                        <Button
                          sx={{
                            fontFamily: 'Apple Color Emoji',
                            color: 'orange',
                          }}
                          onClick={() =>
                            user.id === u.id
                              ? history('/profile')
                              : history(`/profile/${u.id}`)}
                          aria-label="singleView"
                        >
                          {u.username}
                        </Button>{' '}
                        <br />
                      </TableCell>
                    </TableRow>
                  </Table>
                )
              ) : u.displayName ? (
                <Table>
                  {u.displayName} <br />
                </Table>
              ) : (
                <Table>
                  {u.username} <br />
                </Table>
              )
            )
          ) : (
            <Typography sx={{ fontFamily: 'cursive', color: 'orange' }}>
              "No likes yet"
            </Typography>
          )}
        </Box>
      </Modal>
    </div>
  );
};
Share.propTypes = {
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
  cocktail: PropTypes.object.isRequired,
  load: PropTypes.bool.isRequired,
};
export default Share;
