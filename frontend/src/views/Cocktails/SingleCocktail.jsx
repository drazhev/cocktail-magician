import React, { useContext, useEffect, useState } from "react";
import "./SingleCocktail.css";
import { useParams } from "react-router-dom";
import { Container, IconButton, Typography } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import AuthContext from "../../auth/AuthContext";
import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import {
  addToFavorite,
  getAllIngredients,
  removeCocktailIngredient,
  removeFromFavorite,
} from "../../services/requests";
import UsersList from "./UsersList";
import { useNavigate } from "react-router";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import DoneIcon from "@mui/icons-material/Done";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import EditCocktail from "../../components/EditCocktail/EditCocktail";
import AddIcon from "@mui/icons-material/Add";
import AddIngredient from "../../components/AddIngredient/AddIngredient";

function SingleCocktail() {
  const [cocktail, setCocktail] = useState([]);
  const [ingredients, setIngredients] = useState([]);
  const [load, setLoad] = useState(false);
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [openAdd, setOpenAdd] = useState(false);
  const [click, setClick] = useState(false);
  const { id } = useParams();
  const { isLoggedIn, user } = useContext(AuthContext);
  const history = useNavigate();
  const handleOpen = () => setOpen(true);
  const handleClick = () => (click ? setClick(false) : setClick(true));

  const handleClickOpen = () => {
    setOpenEdit(true);
  };

  const handleClickAddOpen = () => {
    setOpenAdd(true);
    getAllIngredients(setIngredients);
  };

  useEffect(() => {
    fetch(`http://localhost:5000/cocktails/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setCocktail(res);
        setLoad(true);
      })
      .catch(({ message }) => {
        console.log(message);
      });
  }, []);

  const deleteIngredient = (ingredient) => {
    removeCocktailIngredient(cocktail.id, [ingredient.name]).then(() => {
      const filtered = cocktail.ingredients.filter(
        (record) => record.name !== ingredient.name
      );
      setCocktail({ ...cocktail, ingredients: filtered });
    });
  };

  if (!load) {
    <div>Loading...</div>;
  }

  return (
    <div className="singleViewCocktail">
      <Container>
        <div align="center">
          <Card sx={{ width: 710, height: 570, display: "-webkit-box" }}>
            <div>
              <CardMedia
                component="img"
                height="250px"
                width="300px"
                image={`http://localhost:5000${cocktail.imageUrl}`}
                alt="cocktail image"
                style={{ height: "100%" }}
              />
            </div>
            {openEdit && (
              <EditCocktail
                openEdit={openEdit}
                setOpenEdit={setOpenEdit}
                cocktail={cocktail}
                setCocktail={setCocktail}
                load={load}
              />
            )}
            {openAdd && (
              <AddIngredient
                openAdd={openAdd}
                setOpenAdd={setOpenAdd}
                cocktail={cocktail}
                setCocktail={setCocktail}
                ingredients={ingredients}
                setIngredients={setIngredients}
                load={load}
              />
            )}
            <CardContent
              style={{ width: "300px", backgroundColor: "rgb(143 115 96)" }}
              align="center"
            >
              <Typography
                variant="h5"
                component="div"
                sx={{ fontFamily: "Apple Color Emoji", fontSize: "xx-large" }}
              >
                {cocktail.name}
              </Typography>
              {load && user && user.role === "magician" && (
                <div>
                  <div>
                    {" "}
                    <IconButton
                      onClick={handleClickOpen}
                      sx={{ fontSize: "small" }}
                    >
                      <EditIcon />
                    </IconButton>
                    Edit Cocktail
                  </div>
                  <div>
                    <IconButton
                      onClick={handleClickAddOpen}
                      sx={{ fontSize: "small" }}
                    >
                      <AddIcon />
                    </IconButton>
                    Add ingredient
                  </div>
                </div>
              )}
              <List
                sx={{
                  width: "100%",
                  maxWidth: 360,
                  bgcolor: "rgb(41, 50, 65)",
                  borderRadius: "5px",
                  color: "white",
                }}
              >
                {load &&
                  cocktail.ingredients.map((ingredient) => (
                    <ListItem
                      sx={{ marginLeft: "5px" }}
                      key={ingredient.name}
                      disableGutters
                      disablePadding
                      secondaryAction={
                        <div>
                          <IconButton sx={{ color: "green" }}>
                            <DoneIcon />
                          </IconButton>
                          {load && user && user.role === "magician" && (
                            <IconButton
                              onClick={() => deleteIngredient(ingredient)}
                              sx={{ color: "orange" }}
                            >
                              <DeleteIcon />
                            </IconButton>
                          )}
                        </div>
                      }
                    >
                      <ListItemText primary={`${ingredient.name}`} />
                    </ListItem>
                  ))}
              </List>
              {!isLoggedIn ? null : cocktail.favouritedBy?.some(
                  (u) => u.id === user.id
                ) ? (
                  <Button>
                    <FavoriteIcon
                      color="error"
                      fontSize="large"
                      onClick={() =>
                      removeFromFavorite(id, cocktail, setCocktail)}
                    />
                  </Button>
              ) : (
                <Button sx={{ color: "pink" }}>
                  <FavoriteBorderIcon
                    fontSize="large"
                    onClick={() => addToFavorite(id, cocktail, setCocktail)}
                  />
                </Button>
              )}
              <br />
              <div className="bottom">
                <div className="bottom-inner">
                  Liked By:
                  <Button
                    size="small"
                    sx={{ color: "orange" }}
                    onClick={handleOpen}
                  >
                    Users
                  </Button>
                  <UsersList
                    open={open}
                    setOpen={setOpen}
                    cocktail={cocktail}
                    load={load}
                  />
                </div>
                <div className="bottom-inner">
                  Available in:
                  <Button
                    size="small"
                    sx={{ color: "orange" }}
                    onClick={handleClick}
                  >
                    Bars
                  </Button>
                </div>
              </div>

              {click
                ? load && cocktail.availableIn.length > 0
                  ? cocktail.availableIn.map((bar) => (
                    <div key={bar.id}>
                      <Button
                        sx={{
                        fontFamily: "emoji",
                        color: "white",
                        fontSize: "large",
                        }}
                        onClick={() => history(`/bars/${bar.id}`)}
                        aria-label="singleView"
                      >
                        {" "}
                        {bar.name}{" "}
                      </Button>
                    </div>
                    ))
                  : "You can't find this cocktail anywhere"
                : null}
            </CardContent>
          </Card>
        </div>
      </Container>
    </div>
  );
}
export default SingleCocktail;
