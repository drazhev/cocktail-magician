import React, { useContext, useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import PropTypes from 'prop-types';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import ListSubheader from '@mui/material/ListSubheader';
import IconButton from '@mui/material/IconButton';
import InfoIcon from '@mui/icons-material/Info';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import { useNavigate } from 'react-router';
import AuthContext from '../../auth/AuthContext';
import EditCocktail from '../../components/EditCocktail/EditCocktail';
import { removeCocktailFromBar, getAllCocktails } from '../../services/requests';
import AddCocktailToBar from '../../components/AddCocktailToBar/AddCocktailToBar';
import './CocktailMenu.css';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 1000,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  borderRadius: '5px',
  backgroundColor: '#9d8a88',
  boxShadow: 24,
  p: 4,
  justifyContent: 'center',
};

// eslint-disable-next-line react/prop-types
const CocktailMenu = ({ open, setOpen, bar, setBar }) => {
  const handleClose = () => setOpen(false);
  const history = useNavigate();
  const { user } = useContext(AuthContext);
  const [cocktails, setCocktails] = useState([]);
  const [openAdd, setOpenAdd] = useState(false);
  const [load, setLoad] = useState(false);

  const handleClickOpen = () => {
    setAddOpen(true);
  };

  const handleClickAddOpen = () => {
    setOpenAdd(true);
  };

  const removeCocktail = (cocktailId) => {
    removeCocktailFromBar(bar.id, cocktailId)
    .then(() => {
      setLoad(true);
      const filtered = bar.cocktails.filter((cocktail) => cocktail.id !== cocktailId);
      setBar({...bar, cocktails: filtered})
    })
  }

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        align="center"
      >
        <Box sx={style}>
          {openAdd && (
            <AddCocktailToBar
              openAdd={openAdd}
              setOpenAdd={setOpenAdd}
              bar={bar}
              setBar={setBar}
              cocktails={cocktails}
              setCocktails={setCocktails}
              load={load}
            />
          )}
          <ImageList className='list-cocktails' sx={{ width: '100%', height: 600 }}>
            <ImageListItem key="Subheader" cols={3}>
              <ListSubheader component="div" sx={{fontSize:'xxx-large', borderRadius:'5px', backgroundColor: "rgb(41, 50, 65)", color:'white', fontFamily:'Apple Color Emoji'}}>List Of Cocktails</ListSubheader>
              {user && user.role === 'magician' && (
                <div style={{justifyContent:'center'}}>
                  <IconButton onClick={handleClickAddOpen} sx={{ width: '40px', fontSize:'small' }}>
                    <AddIcon />
                  </IconButton>Add Cocktail To Bar List
                </div>
              )}
            </ImageListItem>
            {bar.cocktails?.map((item) => (
              <div key={item.imageUrl}>
                <ImageListItem key={item.imageUrl} sx={{ width: '250px' }}>
                  <img
                    src={`http://localhost:5000/${item.imageUrl}?w=248&fit=crop&auto=format`}
                    srcSet={`http://localhost:5000/${item.imageUrl}?w=248&fit=crop&auto=format&dpr=2 2x`}
                    alt={item.name}
                    loading="lazy"
                    style={{ height: '350px', width: '250px' }}
                  />
                  <ImageListItemBar
                    title={item.name}
                    actionIcon={
                      <IconButton
                        onClick={() => history(`/cocktails/${item.id}`)}
                        sx={{ color: 'rgba(255, 255, 255, 0.54)' }}
                        aria-label={`info about ${item.title}`}
                      >
                        <InfoIcon />
                      </IconButton>
                    }
                  />
                  {user && user.role === 'magician' && (
                    <IconButton
                      onClick={() => removeCocktail(item.id)}
                      sx={{
                        width: '40px',
                        marginBottom: '6px',
                        color: 'orange',
                      }}
                    >
                      <DeleteIcon />
                    </IconButton>
                  )}
                </ImageListItem>
              </div>
            ))}
          </ImageList>
        </Box>
      </Modal>
    </div>
  );
};

CocktailMenu.propTypes = {
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
  bar: PropTypes.object.isRequired,
};

export default CocktailMenu;
