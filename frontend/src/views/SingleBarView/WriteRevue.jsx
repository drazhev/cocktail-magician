import React, { useState } from 'react';
import Popover from '@mui/material/Popover';
import Button from '@mui/material/Button';
import {
  getBarReviews,
  getReview,
  postBarReview,
  postReview,
} from '../../services/requests';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';

// eslint-disable-next-line react/prop-types
const WriteReview = ({ barId, update, setUpdate }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [review, setReview] = useState({ text:'',rating:0 });

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const submitReview = () => {
    if (review.text === '' && review.rating === 0) {
      alert('Text and rating should not be empty'); // alert
    }

    postBarReview(barId, review )
    .then(() => {
      getBarReviews(() => {
        setUpdate(!update)
      }, barId)
      .then(() => {
        setAnchorEl(null);
      });
    });
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <div className="write-review" style={{ textAlign: 'center' }}>
      <Button
        aria-describedby={id}
        variant="outlined"
        onClick={handleClick}
        sx={{ color: 'black', borderColor: 'black' }}
      >
        Write A Review
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <Box
          component="form"
          sx={{
            '& > :not(style)': { m: 1, width: '25ch' },
          }}
          noValidate
          autoComplete="off"
        >
          <Box
            sx={{
              '& > legend': { mt: 2 },
            }}
          >
            <Typography component="legend">Rate Bar</Typography>
            <Rating
              name="simple-controlled"
              value={review.rating ?? 0}
              onChange={(event) => {
                setReview({...review, rating: parseInt(event.target.value)});
              }}
            />
          </Box>
        </Box>
        <Box
          component="form"
          sx={{
            '& > :not(style)': { m: 1, width: '50ch' },
          }}
          noValidate
          autoComplete="off"
        >
          <TextField
            onChange={(event) => setReview({ ...review, text: event.target.value})}
            value={review.text}
            label="Review "
            placeholder="Write your review here..."
            variant="outlined"
            sx={{ color: 'black', borderColor: 'black' }}
          />
        </Box>
        <div
          className="submit-review"
          style={{ textAlign: 'center', marginBottom: '5px' }}
        >
          <Button
            aria-describedby={id}
            variant="outlined"
            sx={{ color: 'black', borderColor: 'black' }}
            onClick={(event) => {
              handleClick(event);
              submitReview();
            }}
          >
            Submit
          </Button>
        </div>
      </Popover>
    </div>
  );
};

export default WriteReview;
