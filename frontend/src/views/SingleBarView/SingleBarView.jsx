import React, { useContext, useEffect, useState } from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import LocalBar from '@mui/icons-material/LocalBar';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {
  getBarInfo,
  getBarReviews,
} from '../../services/requests';
import { useParams } from 'react-router';
import './SingleBarView.css';
import CocktailMenu from './CocktailMenu';
import Button from '@mui/material/Button';
import { Container } from '@mui/material';
import StarsIcon from '@mui/icons-material/Stars';
import LocalSeeIcon from '@mui/icons-material/LocalSee';
import WriteReview from './WriteRevue';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useNavigate } from 'react-router';
import AuthContext from '../../auth/AuthContext';
import EditIcon from '@mui/icons-material/Edit';
import EditBar from '../../components/EditBar/EditBar';
import BarGallery from '../../components/BarGallery/BarGallery';

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

const SingleBarView = (props) => {
  const { id } = useParams();
  const [expanded, setExpanded] = useState(false);
  const [bar, setBar] = useState({});
  const [isLoaded, setIsLoad] = useState(false);
  const [open, setOpen] = useState(false);
  const [openEdit, setOpenEdit] = useState(false);
  const [reviews, setReviews] = useState([]);
  const [openRate, setOpenRate] = useState(false);
  const [update, setUpdate] = useState(false);
  const history = useNavigate();
  const { user, isLoggedIn } = useContext(AuthContext);
  const handleRateOpen = () => setOpenRate(true);
  const handleOpen = () => setOpen(true);
  const [openGallery, setOpenGallery] = useState(false);
  const handleOpenGallery = () => setOpenGallery(true);

  const handleClickOpen = () => {
    setOpenEdit(true);
  };
  useEffect(() => {
    Promise.all([getBarInfo(setBar, id), getBarReviews(setReviews, id)]).then(
      () => setIsLoad(true)
    );
  }, [id, update]);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <div className="singleBarView">
      {isLoaded ? (
        <Container>
          <h1
            align="center"
            style={{
              color: 'white',
              fontFamily: 'emoji',
              fontSize: 'xx-large',
            }}
          >
            {bar.name}
          </h1>
          {openEdit && (
            <EditBar
              openEdit={openEdit}
              setOpenEdit={setOpenEdit}
              bar={bar}
              setBar={setBar}
            />
          )}
          <Card className="bar-view" sx={{ maxWidth: '100%' }}>
            {isLoaded && user && user.role === 'magician' ? (
              <CardHeader
                style={{
                  backgroundColor: '#9d8a88',
                  height: '30px',
                }}
                action={
                  <IconButton onClick={handleClickOpen} aria-label="settings">
                    <EditIcon />
                  </IconButton>
                }
                title={`Address: ${bar.address}`}
                subheader={`Book an appointment: ${bar.phone}`}
              />
            ) : (
              <CardHeader
                style={{
                  backgroundColor: '#9d8a88',
                  height: '30px',
                }}
                title={`Address: ${bar.address}`}
                subheader={`Book an appointment: ${bar.phone}`}
              />
            )}
            <CardMedia
              component="img"
              // height="100%"
              sx={{ height:'500px' }}
              image={bar.cover ? `http://localhost:5000${bar.cover}` : null}
              alt="Bar photo"
            />
            {openGallery && (
              <BarGallery
                openGallery={openGallery}
                setOpenGallery={setOpenGallery}
                bar={bar}
                setBar={setBar}
                update={update}
                setUpdate={setUpdate}
              />
            )}
            <CardActions
              disableSpacing
              style={{ backgroundColor: '#9d8a88' }}
            >
              <IconButton aria-label="RateTheBar" onClick={handleRateOpen}>
                <StarsIcon />
                {bar.rating?.toFixed(1)}
              </IconButton>
              <IconButton aria-label="ViewGallery" onClick={handleOpenGallery}>
                <LocalSeeIcon />
              </IconButton>
              <IconButton aria-label="Cocktails" onClick={handleOpen}>
                <LocalBar />
                <CocktailMenu
                  open={open}
                  setOpen={setOpen}
                  bar={bar}
                  setBar={setBar}
                />
              </IconButton>
              <ExpandMore
                expand={expanded}
                onClick={handleExpandClick}
                aria-expanded={expanded}
                aria-label="show more"
              >
                <ExpandMoreIcon />
              </ExpandMore>
              Reviews: {bar.reviewsCount}
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
              <CardContent>
                <Typography
                  sx={{
                    fontFamily: 'Apple Color Emoji',
                    fontSize: 'x-large',
                    textAlign: 'center',
                  }}
                  paragraph
                >
                  Reviews:
                </Typography>
                {bar.reviewsCount === 0 && (
                  <span
                    style={{
                      marginRight: 200,
                      fontFamily: 'Apple Color Emoji',
                    }}
                  >
                    No reviews for this bar yet
                  </span>
                )}
                {reviews.length !== 0 && (
                  <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                      <TableHead>
                        <TableRow>
                          <TableCell>Display Name</TableCell>
                          <TableCell align="right">Content</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {reviews.map((review) => (
                          <TableRow
                            key={review.id}
                            sx={{
                              '&:last-child td, &:last-child th': { border: 0 },
                            }}
                          >
                            <TableCell component="th" scope="row">
                              {isLoggedIn ? (
                                <Button
                                  onClick={() =>
                                    user.id === review.author.id
                                      ? history('/profile')
                                      : history(`/profile/${review.author.id}`)}
                                >
                                  {review.author.displayName
                                    ? review.author.displayName
                                    : review.author.username}
                                </Button>
                              ) : review.author.displayName ? (
                                review.author.displayName
                              ) : (
                                review.author.username
                              )}
                            </TableCell>
                            <TableCell align="right">{review.text}</TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                )}
                {isLoaded && isLoggedIn &&
                  !reviews.some((review) => review.author.id === user.id) && (
                    <div className="write-review" style={{ marginTop: '20px' }}>
                      <WriteReview
                        barId={bar.id}
                        update={update}
                        setUpdate={setUpdate}
                      />
                    </div>
                  )}
              </CardContent>
            </Collapse>
          </Card>
        </Container>
      ) : (
        <div>Loading...</div>
      )}
    </div>
  );
};

export default SingleBarView;
