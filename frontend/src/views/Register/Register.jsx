import React from 'react';
import "./Register.css";
import { useState } from "react";
import { Alert, Snackbar } from "@mui/material";
import { useNavigate } from 'react-router-dom';

export const Register = () => {
  const [user, setUser] = useState({
    username: "",
    password: "",
    confirmPassword: "",
  });

  const history = useNavigate();

  const [message, setMessage] = useState("");
  const [error, setError] = useState("");
  const [open, setOpen] = useState(false);

  const signup = (e) => {
    e.preventDefault();

    if (
      user.username === "" ||
      user.password === "" ||
      user.confirmPassword === ""
    ) {
      setMessage("Username/Password/Confirm password should not be empty!");
      setOpen(true);
      return;
    }

    if (user.password !== user.confirmPassword) {
      setMessage("Password and Confirm Password must be equal!");
      setOpen(true);
      return;
    }
    
    if (user.username.length < 3 || user.username.length > 30) {
      setMessage("Username length should be above 3 symbols and bellow 30 symbols!");
      setOpen(true);
      return;
    }

    if (user.password.length < 5 || user.password.length > 30) {
      setMessage("Password length should be above 5 symbols and bellow 30 symbols!");
      setOpen(true);
      return;
    }

    fetch("http://localhost:5000/users", {
      method: "POST",
      body: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((r) => r.json())
      .then((data) => {
        if (data.message) {
          throw new Error(data.message);
        } else {
          setMessage("Register successfull!");
          setOpen(true);
          setTimeout(() => {
            return history("/login");
          }, 2000);
        }
      })
      .catch(({ message }) => {
        setError(message);
        setOpen(true);
      });
  };
  return (
    <section className="login">
      <div className="login_box">
        <div className="left">
          <div className="top_link" />
          <div className="contact">
            <form action="">
              <h3>SIGN UP</h3>
              <input
                onChange={(e) => setUser({ ...user, username: e.target.value })}
                type="text"
                placeholder="USERNAME"
              />
              <input
                onChange={(e) => setUser({ ...user, password: e.target.value })}
                type="password"
                placeholder="PASSWORD"
              />
              <input
                onChange={(e) =>
                  setUser({ ...user, confirmPassword: e.target.value })}
                type="password"
                placeholder="CONFIRM PASSWORD"
              />
              <button onClick={(e) => signup(e)} className="submit">
                Register
              </button>
            </form>
          </div>
        </div>
        <div className="right signUp" />
      </div>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={open}
        autoHideDuration={6000}
        onClose={() => setOpen(false)}
      >
        <Alert
          onClose={() => setOpen(false)}
          severity="error"
          sx={{ width: "100%" }}
        >
          {error ? error : message}
        </Alert>
      </Snackbar>
    </section>
  );
};

export default Register;
