import React from "react";
import "./MeetTheTeam.css";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Link,
  Typography,
} from "@mui/material";
import hero1 from "../../assets/hero1.jpeg";
import hero2 from "../../assets/hero2.jpeg";

const MeetTheTeam = () => {
  return (
    <div className="about">
      <Card
        sx={{
          width: "100%",
          textAlign: "center",
          backgroundColor: "transparent",
        }}
      >
        <Button
          sx={{
            color: "white",
            mb: 1.5,
            mt: 16,
            fontFamily: "cursive",
          }}
        >
          Buy me a coffee
        </Button>
        <CardContent>
          <Typography
            sx={{
              width: "60%",
              ml: 35,
              fontSize: "x-large",
              fontFamily: "cursive",
            }}
            color="rgb(237 236 239 / 83%)"
          >
            The mission of Tales & Spirits is to present to the seekers of
            interesting drinks the secret bars and hidden venues, where
            experienced bartenders mix innovative cocktails with a casual
            experience, which combines molecular mixology with great service and
            knowledge. Got a question? Let us introduce you to our masters of
            liquid art and feel free to get in touch through the LinkedIn
            accounts below.
          </Typography>
        </CardContent>
      </Card>
      <div className="about-us-cards">
        <Grid
          container
          style={{ justifyContent: "space-around", paddingBottom: "30px" }}
        >
          <Grid item md="auto">
            <Card sx={{ maxWidth: 600, display: "-webkit-box" }}>
              <div>
                <CardMedia
                  component="img"
                  height="426px"
                  width="300px"
                  image={hero1}
                  alt="green iguana"
                />
              </div>
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h5"
                  component="div"
                  textAlign="center"
                  sx={{ fontFamily: "Apple Color Emoji" }}
                >
                  Alexander
                </Typography>
                <Typography
                  variant="body1"
                  color="text.secondary"
                  width="300px"
                  textAlign="left"
                  sx={{ fontFamily: "Apple Color Emoji" }}
                >
                  Former bartender with a passion for Frontend development. Alex
                  is looking for inspiration outside the box and strives to
                  bring sense of simplicity, clarity, authenticity and
                  appropriateness to every project. He believes in working hard,
                  sitting up straight, avoiding cliche, embracing the
                  unexpected. He also love mixing medications and in between
                  these moments Alex can be found around the vinyl stores,
                  exploring funky and jazzy grooves.
                </Typography>
                <CardActions>
                  <Link to="http://linkedin.com" size="small">
                    LinkedIn
                  </Link>
                </CardActions>
              </CardContent>
            </Card>
          </Grid>
          <Grid item md="auto">
            <Card
              sx={{
                maxWidth: 600,
                justifyContent: "center",
                display: "-webkit-box",
              }}
            >
              <div>
                <CardMedia
                  component="img"
                  height="426px"
                  width="300px"
                  image={hero2}
                  alt="green iguana"
                />
              </div>
              <CardContent>
                <Typography
                  gutterBottom
                  variant="h5"
                  component="div"
                  textAlign="center"
                  sx={{ fontFamily: "Apple Color Emoji" }}
                >
                  Ivo
                </Typography>
                <Typography
                  variant="body1"
                  color="text.secondary"
                  width="300px"
                  textAlign="left"
                  letterSpacing="inherit"
                  sx={{ fontFamily: "Apple Color Emoji" }}
                >
                  With a passion for interactive design, Ivo loves to create
                  beautiful digital landscapes and then he maps out the
                  interactive components. Ivo sees every new project as an
                  opportunity to raise the bar. Creating memorable web pages is
                  the product of hard thinking, collaboration and the odd genius
                  moment. In between these moments Ivo can be found hiking the
                  Rila, Pirin or Rhodope Mountains, always with a bottle of
                  scotch in his backpack.
                </Typography>
                <CardActions>
                  <Link size="small">LinkedIn</Link>
                </CardActions>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default MeetTheTeam;
