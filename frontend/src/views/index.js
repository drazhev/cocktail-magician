import Home from "./Home/Home";
import MeetTheTeam from "./MeetTheTeam/MeetTheTeam";
import SingleBarView from "./SingleBarView/SingleBarView";
import SingleCocktail from "./Cocktails/SingleCocktail";
import Bars from "./Bars/Bars";
import Cocktails from "./Cocktails/Cocktails";
import NotFound from "./NotFound/NotFound";
import Register from "./Register/Register";
import Login from "./Login/Login";
import Profile from './Profile/Profile';
import MyProfile from './MyProfile/MyProfile';
import Admin from './Admin/Admin';


export {
    Home, MeetTheTeam, SingleBarView, SingleCocktail, Bars, Cocktails, NotFound, Register, Login, Profile, MyProfile, Admin
};
