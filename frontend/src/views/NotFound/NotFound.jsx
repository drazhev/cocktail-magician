import React from "react";
import errorImg from "../../assets/error.jpg";

const NotFound = () => {
  return (
    <div style={{backgroundColor: "gray"}}>
      <img src={errorImg} style={{ width: "100%", marginTop: "100px"}} alt="Error" />
    </div>
  );
};

export default NotFound;
