import React, { useEffect, useState } from "react";
import UserInfoMeCard from "../../components/UserInfoMeCard/UserInfoMeCard";
import { getUserInfo } from "../../services/requests";
import "./MyProfile.css"

const MyProfile = () => {
  const [user, setUser] = useState({});
  const [isLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    getUserInfo()
    .then((response) => {
      setUser(response)
      setIsLoaded(true)
    })
  })

  return (
    <div className="myProfile">
      {isLoaded && 
        <UserInfoMeCard user={user} isLoaded={isLoaded} />}
    </div>
  )
}

export default MyProfile;
