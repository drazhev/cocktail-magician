import React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import background from '../../assets/background.png'
import { useNavigate } from 'react-router';
import './Elixirs.css'

const Elixirs = () => {
  const history = useNavigate();

  return (
    <Card className="elixirs" sx={{ minWidth: 275, width: "100%", height: "365px", textAlign:"center" }}>
      <CardContent>
        <Typography sx={{ mb: 1.5, width:"70%", ml:"200px", fontFamily: 'cursive', fontSize: 'x-large' }} color="rgb(237 236 239 / 83%)">
          We’re masters in the dark arts of molecular mixology. 
          Our venues are a safe haven for distinctive drinkers from all around the world. 
          Ever since the founding of this illustrious Tales&Spirits practice in November 2021, 
          experienced bartenders have been dispensing free psychological advice along with a 
          broad range of tasty medications. Our mixologists create every cocktail with an obsessive eye 
          for detail, presented in vessels orchestrated to add a devilish dash of theatre, they bedazzle, 
          bewitch and set the scene for everything we do. Step into the mystical apothecary, 
          dispensing the most creative elixirs. 
        </Typography>
      </CardContent>
      <CardActions sx={{ justifyContent:"center" }}>
        <Button onClick={() => history("cocktails")} size="medium" variant="outlined" sx={{ color:"#a3babd", border:"1px solid #a3babd", fontFamily: 'Apple Color Emoji', fontSize: 'large' }}>Potions, Elixirs & Medications</Button>
      </CardActions>
    </Card>
  );
}

export default Elixirs;
