import React from "react";
import Intro from './IntroView';
import Venues from './Venues';
import BestBars from './BestBars'
import Elixirs from "./Elixirs";
import BestCocktails from "./BestCocktails";

const Home = () => {
  
  return (
    <div>
      <Intro />
      <Venues />
      <BestBars />
      <Elixirs />
      <BestCocktails />
    </div>
  )
}

export default Home;
