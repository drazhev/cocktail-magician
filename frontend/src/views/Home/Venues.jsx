import React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import './Venues.css';
import { useNavigate } from 'react-router';

const Venues = () => {
  const history = useNavigate();

  return (
    <Card className="venues" sx={{ minWidth: 275, height: "220px", textAlign:"center", mt: "-5px" }}>
      <CardContent>
        <Typography
          sx={{ mb: 1.5, width:"70%", ml:"200px", fontFamily: 'cursive', fontSize: 'x-large' }}
          color="#a3babd"
          gutterBottom
        >
          Home to masters in the dark arts of molecular mixology. If you’re in
          need of treatment don’t hesitate to make an appointment in some of our
          secret and hidden venues. Step into our world of theatre served.
        </Typography>
      </CardContent>
      <CardActions sx={{ justifyContent:"center" }}>
        <Button onClick={() => history("bars")} size="medium" variant="outlined" sx={{ color:"#a3babd", border:"1px solid #a3babd",fontFamily: 'Apple Color Emoji', fontSize: 'large' }}>Our Bars</Button>
      </CardActions>
    </Card>
  );
};

export default Venues;
