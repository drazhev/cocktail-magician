import introVideo from '../../assets/intro-video.mp4';
import React from 'react';
import './IntroView.css'

const Intro = () => {
  
  return (
    <div className="intro-video" style={{background: 'black'}}>
      <video style={{opacity: '0.5'}} width='100%' loop autoPlay muted>
        <source src={introVideo} type="video/mp4" />
      </video>
      <div className='textOnImage' style={{textAlign:'left', fontFamily: 'Apple Color Emoji', fontSize: 'large'}}>
        <h1>EXPERIENCES FILLED WITH<br /> WONDER & DELIGHT </h1>
        <p> Theatre served </p>
      </div>
    </div>
  );
};

export default Intro;
