import React, { useContext } from 'react';
import "./Login.css";
import { useState } from "react";
import AuthContext from "../../auth/AuthContext";
import jwt from "jsonwebtoken";
import { Snackbar, Alert } from "@mui/material";
import { useNavigate } from 'react-router-dom';
import { getUserInfo } from '../../services/requests'


const Login = () => {
  const [user, setUser] = useState({
    username: "",
    password: "",
  });

  const history = useNavigate();

  const [message, setMessage] = useState("");
  const [error, setError] = useState("");
  const [open, setOpen] = useState(false);

  const { setAuth } = useContext(AuthContext);

  const login = (e) => {
    e.preventDefault();

    if (user.username === "" || user.password === "") {
      setMessage("Username/password should not be empty!");
      setOpen(true);
      return;
    }

    fetch("http://localhost:5000/users/login", {
      method: "POST",
      body: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((r) => r.json())
      .then((data) => {
        if (data.message) {
          throw new Error(data.message);
        }
          localStorage.setItem("token", data.token);
          getUserInfo()
            .then(user => {
              setAuth({ user, isLoggedIn: true });
              localStorage.setItem("user", JSON.stringify(user))
            })
      })
      .then(() => history("/"))
      .catch(({ message }) => {
        setError(message);
        setOpen(true);
      });
  };

  return (
    <section className="login">
      <div className="login_box">
        <div className="left">
          <div className="top_link" />
          <div className="contact">
            <form action="">
              <h3>LOG IN</h3>
              <input
                onChange={(e) => setUser({ ...user, username: e.target.value })}
                type="text"
                placeholder="USERNAME"
              /> 
          
              <input
                onChange={(e) => setUser({ ...user, password: e.target.value })}
                type="password"
                placeholder="PASSWORD"
              /> 
              <button onClick={(e) => login(e)} className="submit">
                LETS GO
              </button>
            </form>
          </div>
        </div>
        <div className="right" />
      </div>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={open}
        autoHideDuration={6000}
        onClose={() => setOpen(false)}
      >
        <Alert
          onClose={() => setOpen(false)}
          severity="error"
          sx={{ width: "100%" }}
        >
          {error ? error : message}
        </Alert>
      </Snackbar>
    </section>
  );
};

export default Login;
