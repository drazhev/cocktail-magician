import React, { useEffect, useState } from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import { Button, Container, TextField } from '@mui/material';
import { getToken } from '../../auth/AuthContext';

const Ingredients = () => {
  const [ingredients, setIngredients] = useState([]);
  const [input, setInput] = useState([]);
  const [inputValue, setInputValue] = useState('');

  const deleteIngredient = (id) => {
    fetch(`http://localhost:5000/ingredients/${id}`, {
      method: 'DELETE',
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    })
      .then((r) => setIngredients(ingredients.filter((i) => i.id !== id)))
      .catch(console.error);
  };

  const createIngredient = (e) => {
    e.preventDefault();

    fetch(`http://localhost:5000/ingredients`, {
      method: 'POST',
      body: JSON.stringify(input),
      headers: {
        authorization: `Bearer ${getToken()}`,
        'content-type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setIngredients([...ingredients, { ...res, cocktailsCount: 0 }]);
        setInput([]);
        setInputValue('');
      });
  };
  useEffect(() => {
    fetch('http://localhost:5000/ingredients', {
      method: 'GET',
      headers: {
        authorization: `Bearer ${getToken()}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setIngredients(res);
      })
      .catch(({ message }) => {
        console.log(message);
      });
  }, []);

  return (
    <Container sx={{ paddingTop: '50px', paddingBottom: '35px' }} align="right">
      <input
        onChange={(e) => {
          setInput({ name: e.target.value });
          setInputValue(e.target.value);
        }}
        type="text"
        value={inputValue}
        placeholder="Ingredient"
        style={{ marginRight: '100px' }}
      /><br /><br />
      <Button
        sx={{
          color: 'white',
          borderColor: 'white',
          marginRight: '60px',
          fontSize: 'large',
          fontFamily: 'Apple Color Emoji',
        }}
        variant="outlined"
        onClick={(e) => {
          createIngredient(e);
        }}
      >
        Create Ingredient
      </Button>
      <List
        sx={{
          width: '100%',
          maxWidth: 360,
          bgcolor: 'background.paper',
          marginTop: '35px',
          marginBottom: '30px',
        }}
      >
        {ingredients.map((i) => (
          <ListItem
            key={i.id}
            secondaryAction={
              i.cocktailsCount < 1 && (
                <IconButton onClick={() => deleteIngredient(i.id)}>
                  <DeleteIcon />
                </IconButton>
              )
            }
          >
            <ListItemText primary={i.name} />
          </ListItem>
        ))}
      </List>
    </Container>
  );
};

export default Ingredients;
