import React, { useEffect, useState } from "react";
import { getToken } from "../../auth/AuthContext";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Checkbox from "@mui/material/Checkbox";
import { Alert, Snackbar, Button, Typography } from '@mui/material';

const CreateCocktail = () => {
  const [checked, setChecked] = useState([]);
  const [ingredients, setIngredients] = useState([]);
  const [open, setOpen] = useState(false);
  const [error, setError] = useState("");
  const [message, setMessage] = useState("");

  useEffect(() => {
    fetch("http://localhost:5000/ingredients", {
      method: "GET",
      headers: {
        authorization: `Bearer ${getToken()}`,
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setIngredients(res);
      })
      .catch(({ message }) => {
        setError(message);
        setOpen(true);
      });
  }, []);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const createCocktail = (event) => {
    const formData = new FormData(event.target);

    checked.forEach((e) => formData.append("ingredients[]", e));

    if (formData.get('name').length < 2 || formData.get('name').length > 50) {
      setMessage("Cocktail name must be in range from 2 to 50!");
      setOpen(true);
      return;
    }

    if (formData.get('image').name === '') {
      setMessage("You should add image!");
      setOpen(true);
      return;
    }

    return fetch(`http://localhost:5000/cocktails`, {
      method: "POST",
      body: formData,
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    })
      .then((r) => r.json())
      .then((r) => {
        if (r.message) {
          throw new Error(r.message);
        }
        setMessage('You create cocktail succesfully!');
        setOpen(true);
        event.target.reset();
      })
      .catch(console.error);
  };

  return (
    <div style={{paddingBottom:'20px'}}>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          createCocktail(event);
        }}
      >
        <div style={{paddingTop:'20px', paddingBottom:'70px', paddingLeft:'395px'}}>
          <List
            sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
          >
            {ingredients.map((i) => {
              const labelId = `checkbox-list-label-${i.id}`;

              return (
                <ListItem
                  key={i.id}
                  disablePadding
                >
                  <ListItemButton
                    role={undefined}
                    onClick={handleToggle(i.name)}
                    dense
                  >
                    <ListItemIcon>
                      <Checkbox
                        edge="start"
                        checked={checked.indexOf(i.name) !== -1}
                        tabIndex={-1}
                        disableRipple
                        inputProps={{ "aria-labelledby": labelId }}
                      />
                    </ListItemIcon>
                    <ListItemText id={labelId} primary={i.name} />
                  </ListItemButton>
                </ListItem>
              );
            })}
          </List>
        </div>
        <div style={{display:'inline-grid'}}>
          <Typography sx={{fontFamily:'Apple Color Emoji', fontSize:'x-large'}}>
            Cocktail Name: 
          </Typography>
          <input type="text" name="name" />
          <Typography sx={{fontFamily:'Apple Color Emoji', fontSize:'x-large'}}>
            Cocktail Image: 
          </Typography>
          <input type="file" name="image" />
        </div><br /><br />

        <Button sx={{fontFamily:'Apple Color Emoji', fontSize:'large', color:'white', borderColor:'white'}} className="create-button" type="submit" variant="outlined">
          Create Cocktail
        </Button>

      </form>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={open}
        autoHideDuration={6000}
        onClose={() => setOpen(false)}
      >
        <Alert
          onClose={() => setOpen(false)}
          severity="error"
          sx={{ width: "100%" }}
        >
          {error ? error : message}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default CreateCocktail;
