import { Button, Container, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React, { useContext, useState } from "react";
import AuthContext from '../../auth/AuthContext';
import "./Admin.css";
import CreateBar from './CreateBar';
import CreateCocktail from './CreateCocktail';
import Ingredients from './Ingredients';

const Admin = () => {
  const { user } = useContext(AuthContext);
  const [active, setActive] = useState('bar');

  return (
    <div className="admin">
      <Container>
        <Typography sx={{fontFamily:'cursive', fontSize:'x-large', color:'white'}}>Hello, {user.displayName ? user.displayName : user.username}.
          Here you can manage your website.
        </Typography>
        <div className="admin-buttons" style={{paddingTop:'60px'}}>
          <Button sx={{fontFamily:'Apple Color Emoji', fontSize:'x-large', border:'1px solid white', color:'orange'}} onClick={() => setActive("bar")} className={active === "bar" ? "activeBtn" : ""}>Create Bar</Button>
          <Button sx={{fontFamily:'Apple Color Emoji', fontSize:'x-large', border:'1px solid white', color:'orange'}} onClick={() => setActive("cocktail")} className={active === "cocktail" ? "activeBtn" : ""}>Create Cocktail</Button>
          <Button sx={{fontFamily:'Apple Color Emoji', fontSize:'x-large', border:'1px solid white', color:'orange'}} onClick={() => setActive("ingredients")} className={active === "ingredients" ? "activeBtn" : ""}>Ingredients</Button>
        </div>
        
        {active === "bar" &&
          <CreateBar />}
        {active === "cocktail" &&
          <CreateCocktail />}
        {active === "ingredients" &&
          <Ingredients />}
      </Container>
    </div>
  )
};

export default Admin;
