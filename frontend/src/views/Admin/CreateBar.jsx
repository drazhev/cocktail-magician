import { Alert, Button, Snackbar, Container } from "@mui/material";
import React, { useState } from "react";
import { getToken } from "../../auth/AuthContext";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { setBarCover } from "../../services/requests";

const CreateBar = () => {

  const [open, setOpen] = useState(false);
  const [error, setError] = useState("");
  const [message, setMessage] = useState("");

  const createBar = (event) => {
    const formData = new FormData(event.target);

    if (formData.get('name').length < 5 || formData.get('name').length > 50) {
      setMessage("Bar name must be in range from 5 to 50!");
      setOpen(true);
      return;
    }

    if (formData.get('address').length < 5 || formData.get('address').length > 100) {
      setMessage("Bar address must be in range from 5 to 100!");
      setOpen(true);
      return;
    }

    if (formData.get('phone').length < 2 || formData.get('phone').length > 20) {
      setMessage("Bar phone must be in range from 2 to 20!");
      setOpen(true);
      return;
    }

    if (formData.get('images').name === '') {
      setMessage("You should add minimum 1 image!");
      setOpen(true);
      return;
    }

    return fetch(`http://localhost:5000/bars`, {
      method: "POST",
      body: formData,
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    })
      .then((r) => r.json())
      .then((r) => {
        if (r.message) {
          throw new Error(r.message);
        }
        setBarCover(r.id, r.images[0].id);
        setMessage('You create bar succesfully!');
        setOpen(true);
        event.target.reset();
      } )
      .catch(({ message }) => {
        setError(message);
        setOpen(true);
      });
  };

  return (
    <Container sx={{paddingTop:'30px', paddingBottom:'190px', width:'800px', textAlign:'left', marginRight:'300px'}} align="left">
      <form
        onSubmit={(event) => {
          event.preventDefault();
          createBar(event);
        }}
      >
        <TableContainer sx={{marginTop:'80px', marginBottom:'20px', width:'112%'}} component={Paper}>
          <Table sx={{ minWidth: 750 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="left">Bar Name</TableCell>
                <TableCell align="left">Bar Phone</TableCell>
                <TableCell align="left">Bar Address</TableCell>
                <TableCell align="left">Bar Images</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow
                key={message}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell align="right">
                  <input type="text" name="name" />
                </TableCell>
                <TableCell align="right">
                  <input type="text" name="phone" />
                </TableCell>
                <TableCell align="right">
                  <input type="text" name="address" />
                </TableCell>
                <TableCell style={{textAlign:'left'}}>
                  <input type="file" multiple name="images" />
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
        <Button sx={{fontFamily:"Apple Color Emoji", fontSize:'large', color:'white', borderColor:'white'}} className="create-button" type="submit" variant="outlined">
          Create Bar
        </Button>
      </form>
      <Snackbar
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
        open={open}
        autoHideDuration={6000}
        onClose={() => setOpen(false)}
      >
        <Alert
          onClose={() => setOpen(false)}
          severity="error"
          sx={{ width: "100%" }}
        >
          {error ? error : message}
        </Alert>
      </Snackbar>
    </Container>
  );
};

export default CreateBar;
