import React from 'react';
import UserProfileCard from '../../components/UserProfileCard/UserProfileCard';
import './Profile.css';

const Profile = () => {
  return (
    <div className="Profile">
      <UserProfileCard />
    </div>
  );
};

export default Profile;
