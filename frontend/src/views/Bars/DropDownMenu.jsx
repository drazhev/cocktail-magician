import { FormControl, InputLabel, MenuItem, Select } from '@mui/material';
import { Box } from '@mui/system';
import * as React from 'react';
import './DropDownMenu.css'
import PropTypes from "prop-types";

const DropDownMenu = ({value, setValue}) => {

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <Box sx={{ minWidth: 120, textAlign: "center" }}>
      <FormControl className="searchBy" sx={{ width: "200px" }}>
        <InputLabel sx={{ color: "gray" }} id="demo-simple-select-label">
          Search By
        </InputLabel>
        <Select
          sx={{ color: "gray" }}
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={value}
          onChange={(e) => handleChange(e)}
        >
          <MenuItem value={1}>By Name</MenuItem>
          <MenuItem value={2}>By Ingredients</MenuItem>
        </Select>
      </FormControl>
    </Box>
  )
}

DropDownMenu.propTypes = {
  value: PropTypes.string.isRequired,
  setValue: PropTypes.func.isRequired,
};
export default DropDownMenu;
