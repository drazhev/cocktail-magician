import * as React from 'react';
import './TableByIngredients.css'
import PropTypes from "prop-types";
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useNavigate } from 'react-router';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const TableByIngredients = ({ingredient}) => {
  const history = useNavigate();

  return (
    <div style={{fontFamily:'Apple Color Emoji', color:'white', fontSize:'xx-large',paddingTop:'50px', paddingBottom:'121px'}}>Ingredient: {ingredient.ingredientName}
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead sx={{backgroundColor:'orange'}}>
            <TableRow>
              <StyledTableCell sx={{paddingLeft:'15px'}}>Cocktail</StyledTableCell>
              <StyledTableCell sx={{paddingLeft:'15px'}}>Bar</StyledTableCell>
              <StyledTableCell sx={{paddingLeft:'15px'}}>Bar Phone</StyledTableCell>
              <StyledTableCell sx={{paddingLeft:'15px'}}>Bar Address</StyledTableCell>


            </TableRow>
          </TableHead>
          <TableBody>
            {ingredient.bars.map((i) => (
              <StyledTableRow key={i.cocktailId}>
                <StyledTableCell component="th" scope="row">
                  <span className="cocktailNameButton" onClick={() => history(`/cocktails/${i.cocktailId}`)}>{i.cocktailName}</span>
                </StyledTableCell>
                <StyledTableCell align="left">
                  <span className="barNameButton" onClick={() => history(`/bars/${i.barId}`)}>{i.barName}</span>
                </StyledTableCell>
                <StyledTableCell align="left">{i.phone}</StyledTableCell>
                <StyledTableCell align="left">{i.address}</StyledTableCell>
              </StyledTableRow>
          ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

TableByIngredients.propTypes = {
  ingredient: PropTypes.object.isRequired,
};

export default TableByIngredients;
