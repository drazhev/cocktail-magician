import * as React from 'react';
import PropTypes from "prop-types";
import { Box } from '@mui/system';
import { Button, TextField } from '@mui/material';
import './SearchByName.css'
import SearchIcon from '@mui/icons-material/Search';

const SearchByName = ({setResultByName, setResultByIngredients}) => {
  const [input, setInput] = React.useState("");

  const searchBars = (value) => {
    return fetch(`http://localhost:5000/bars?name=${value}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setResultByName(res);
        setResultByIngredients([]);
      })
      .catch(({ message }) => {
        console.log(message);
      });
  };

  return (
    <Box
      component="form"
      sx={{
      "& > :not(style)": { m: 1, width: "25ch" },
      paddingTop:'25px'
    }}
      noValidate
      autoComplete="off"
    >
      <TextField
        className="searchByName"
        value={input}
        id="outlined-basic"
        label="Outlined"
        variant="outlined"
        onChange={(e) => setInput(e.target.value)}
      />
      <Button
        variant="outlined"
        className="btnSearchBy"
        onClick={() => {
        searchBars(input);
        setInput("");
      }}
      >
        <SearchIcon />
        Search
      </Button>
    </Box>
  )
}

SearchByName.propTypes = {
  setResultByName: PropTypes.func.isRequired,
  setResultByIngredients: PropTypes.func.isRequired
};

export default SearchByName;
