import React, { useEffect, useState } from "react";
import "./Bars.css";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import ImageListItemBar from "@mui/material/ImageListItemBar";
import ListSubheader from "@mui/material/ListSubheader";
import IconButton from "@mui/material/IconButton";
import InfoIcon from "@mui/icons-material/Info";
import { Button, Container, Typography } from "@mui/material";
import { getAllBars } from "../../services/requests";
import { useNavigate } from "react-router";
import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/Inbox";
import TableByIngredient from './TableByIngredient'
import DropDownMenu from './DropDownMenu';
import SearchByName from './SearchByName';
import SearchByIngredient from './SearchByIngredient';

const Bars = () => {
  const [bars, setBars] = useState([]);
  const [value, setValue] = useState("");
  const [resultByName, setResultByName] = useState([]);
  const [resultByIngredients, setResultByIngredients] = useState([]);

  const history = useNavigate();

  useEffect(() => {
    getAllBars(setBars);
  }, []);

  return (
    <div className="bars">
      <Container sx={{ textAlign: "center" }}>
        <ImageList
          className="list-of-bars"
          cols={1}
          gap={10}
        >
          <ImageListItem key="Subheader" cols={1}>
            <ListSubheader
              component="div"
              sx={{
                textAlign: "center",
                backgroundColor: "#0f1112",
                color: "#a3babd",
                fontSize: "xx-large",
                marginBottom: "30px",
                fontFamily: "Apple Color Emoji"
              }}
            >
              Our Bars
            </ListSubheader>
            <Typography
              sx={{
                textAlign: "center",
                fontSize: "x-large",
                color: "#a3babd",
                marginBottom: "20px",
                fontFamily: "cursive"
              }}
            >
              Step into our venues & into our world of theatre served
            </Typography>
          </ImageListItem>
          <DropDownMenu value={value} setValue={setValue} />
          {value === 1 && (
            <SearchByName setResultByName={setResultByName} setResultByIngredients={setResultByIngredients} />
          )}
          {value === 2 && (
            <SearchByIngredient setResultByName={setResultByName} setResultByIngredients={setResultByIngredients} />
          )}
          {resultByName &&
            resultByName.map((bar) => (
              <ImageListItem key={bar.id}>
                <img
                  src={`http://localhost:5000$${bar.url}`}
                  srcSet={`http://localhost:5000${bar.url}`}
                  alt={bar.name}
                  loading="lazy"
                  style={{ height: "600px" }}
                />
                <ImageListItemBar
                  sx={{ top: "0", bottom: "auto" }}
                  title={bar.name}
                  subtitle={`${bar.address}; ${bar.phone}`}
                  actionIcon={
                    <IconButton
                      onClick={() => history(`/bars/${bar.id}`)}
                      sx={{ color: "rgba(255, 255, 255, 0.54)" }}
                      aria-label={`info about ${bar.name}`}
                    >
                      <InfoIcon />
                    </IconButton>
                  }
                />
              </ImageListItem>
            ))}
          
          {resultByIngredients &&
            resultByIngredients.map((ingredient, i) => {
              return (ingredient.bars.length > 0 ?
                <TableByIngredient key={i} ingredient={ingredient} /> : 
                <div style={{color: 'white', paddingBottom: '45px'}}>{`There is no bars where you can find cocktails with: ${ingredient.ingredientName}`}</div>
  )})}
          {resultByName.length === 0 && resultByIngredients.length === 0 && bars.map((bar) => (
            <ImageListItem key={bar.id}>
              <img
                src={`http://localhost:5000${bar.url}`}
                srcSet={`http://localhost:5000${bar.url}`}
                alt={bar.name}
                loading="lazy"
                style={{ height: "600px" }}
              />
              <ImageListItemBar
                sx={{ top: "0", bottom: "auto" }}
                title={bar.name}
                subtitle={`${bar.address}; ${bar.phone}`}
                actionIcon={
                  <IconButton
                    onClick={() => history(`/bars/${bar.id}`)}
                    sx={{ color: "rgba(255, 255, 255, 0.54)" }}
                    aria-label={`info about ${bar.name}`}
                  >
                    <InfoIcon />
                  </IconButton>
                }
              />
            </ImageListItem>
          ))}
        </ImageList>
      </Container>
    </div>
  );
};

export default Bars;
