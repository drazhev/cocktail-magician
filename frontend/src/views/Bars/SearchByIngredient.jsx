import React, { useState } from "react";
import PropTypes from "prop-types";
import { Box } from "@mui/system";
import {
  Button,
  List
} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import AddIcon from '@mui/icons-material/Add';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import FilledInput from '@mui/material/FilledInput';

const SearchByIngredient = ({setResultByName, setResultByIngredients}) => {
  const [ingredient, setIngredient] = useState("");
  const [ingredientList, setIngredientList] = useState([]);

  const searchIngredients = () => {
    let params = "";
    ingredientList.map((ing) => (params += `values=${ing}&`));

    return fetch(`http://localhost:5000/bars/cocktail-ingredients?${params}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setResultByName([]);
        setResultByIngredients(res);
        setIngredientList([]);
      })
      .catch(({ message }) => {
        console.log(message);
      });
  };

  return (
    <div>
      <Box
        component="form"
        noValidate
        autoComplete="off"
      >
        <FilledInput
          value={ingredient}
          placeholder="Ingredient"
          sx={{
            color:'white',
            "&:after": {
              // focused
              borderBottomColor: "red"
            },
            "&:before": {
              borderBottomColor: "orange"
            },
            "&:hover:not(.Mui-focused):before": {
              borderBottomColor: "green"
            },
        }}
          onChange={(e) => setIngredient(e.target.value)}
        />
        <Button
          sx={{
            color: 'white',
          }}
          onClick={() => {
            setIngredientList([...ingredientList, ingredient]);
            setIngredient("");
          }}
        >
          <AddIcon />
          Add ingredient
        </Button>
      </Box>
      <Box
        sx={{
          width: "100%",
          maxWidth: 360,
          bgcolor: "background.paper",
          marginLeft:'320px'
        }}
      >
        <nav aria-label="main mailbox folders" style={{display:'flex', justifyContent:'space-evenly'}}>
          {ingredientList.map((i) => {
            return (
              <List key={i}>
                <Stack direction="row" spacing={1}>
                  <Chip sx={{backgroundColor:'white'}} label={i} />
                </Stack>
              </List>
            );
          })}
        </nav>
      </Box>
      <Button onClick={() => searchIngredients()} sx={{color:'white', borderColor: '1px'}}>
        <SearchIcon />Search
      </Button>
    </div>
  );
};

SearchByIngredient.propTypes = {
  setResultByName: PropTypes.func.isRequired,
  setResultByIngredients: PropTypes.func.isRequired,
};
export default SearchByIngredient;
