import React, { useState } from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import PropTypes from 'prop-types';
import PreviewIcon from '@mui/icons-material/Preview';
import { useNavigate } from 'react-router';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import DoneIcon from '@mui/icons-material/Done';

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

function CardViewCocktails(props) {
  const [expanded, setExpanded] = useState(false);

  const history = useNavigate();

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardHeader
        title={props.cocktail.name}
        sx={{ backgroundColor: '#9d8a88' }}
      />
      <CardMedia
        component="img"
        height="194"
        image={`http://localhost:5000/${props.cocktail.imageUrl}`}
        alt="cocktail pic"
        sx={{ height: '350px' }}
      />
      <CardActions disableSpacing sx={{ backgroundColor: '#9d8a88' }}>
        <IconButton
          onClick={() => history(`/cocktails/${props.cocktail.id}`)}
          aria-label="singleView"
        >
          <PreviewIcon />
        </IconButton>
        <Typography sx={{marginLeft:'35px', marginTop:'16px', fontFamily:'emoji'}} paragraph>
          Liked by {props.cocktail.favouriteCount} user/s
        </Typography>
        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography sx={{fontFamily:'emoji', fontSize:'large'}} paragraph>Ingredients:</Typography>
          {/* i.name + ' ' */}
          <List
            sx={{
              width: '100%',
              maxWidth: 360,
              bgcolor: 'background.paper',
            }}
          >
            {props.cocktail.ingredients.map((ingredient) => (
              <ListItem
                key={ingredient.name}
                disableGutters
                secondaryAction={
                  <DoneIcon />
                }
              >
                <ListItemText primary={`${ingredient.name}`} />
              </ListItem>
            ))}
          </List>
        </CardContent>
      </Collapse>
    </Card>
  );
}

CardViewCocktails.propTypes = {
  cocktail: PropTypes.object.isRequired,
};
export default CardViewCocktails;
