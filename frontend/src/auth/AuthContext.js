import { createContext } from "react";

export const AuthContext = createContext({
  user: {},
  isLoggedIn: false,
  setAuth: () => {},
});

export const getToken = () => localStorage.getItem("token") || "";

export const getUser = () => JSON.parse(localStorage.getItem("user"));

export default AuthContext;
