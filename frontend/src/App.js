import "./App.css";
import React, { useContext, useState } from "react";
import NavBar from "./components/NavBar/NavBar";
import { BrowserRouter as Router, Routes, Route, Navigate, Outlet } from "react-router-dom";
import AuthContext, { getUser } from "./auth/AuthContext";
import AppFooter from './components/AppFooter/AppFooter'
import { Home, MeetTheTeam, SingleBarView, SingleCocktail, Bars, Cocktails, NotFound, Register, Login, Profile, MyProfile, Admin } from './views';

const PrivateRoute = () => {
  const { isLoggedIn } = useContext(AuthContext)

  return isLoggedIn ? <Outlet /> : <Navigate to="/login" />;
}

const AdminRoute = () => {
  const { user } = useContext(AuthContext)

  return user.role === "magician" ? <Outlet /> : <Navigate to="/NotFound" />;
}


function App() {
  const [{ isLoggedIn, token, user }, setAuth] = useState({
    user: getUser(),
    isLoggedIn: !!getUser(),
  });

  return (
    <AuthContext.Provider value={{ isLoggedIn, token, user, setAuth }}>
      <Router>
        <NavBar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/register" element={<Register />} />
          <Route exact path="/cocktails" element={<Cocktails />} />
          <Route exact path="/bars" element={<Bars />} />
          <Route exact path="/about" element={<MeetTheTeam />} />
          <Route exact path="/cocktails/:id" element={<SingleCocktail />} />
          <Route exact path="/bars/:id" element={<SingleBarView />} />
          <Route exact path="/cocktails/:id" element={<SingleCocktail />} />
          <Route exact path="/profile" element={<PrivateRoute />}>
            <Route path="" element={<MyProfile />} />
          </Route>
          <Route exact path="/profile/:id" element={<PrivateRoute />}>
            <Route path="" element={<Profile />} />
          </Route>
          <Route exact path="/admin" element={<AdminRoute />}>
            <Route path="" element={<Admin />} />
          </Route>

          <Route path="*" element={<NotFound />} />
        </Routes>
        <AppFooter />
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
