import { getToken } from "../auth/AuthContext";
import { getUser } from "../auth/AuthContext";

export const getUserInfo = () => {

  return fetch("http://localhost:5000/users/me", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      authorization: `Bearer ${getToken()}`,
    },
  })
    .then((res) => res.json())
    .catch((error) => console.log(error));
};

export const removeFromFavorite = (id, cocktail, setCocktail) => {
  fetch(`http://localhost:5000/users/cocktails/${id}`, {
    method: "DELETE",
    headers: {
      authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => {
      const user = getUser();
      setCocktail({...cocktail, favouritedBy: cocktail.favouritedBy.filter(u => u.id !== user.id)})
    })
    .catch(console.error);
};

export const addToFavorite = (id, cocktail, setCocktail) => {
  fetch(`http://localhost:5000/users/cocktails/${id}`, {
    method: "PUT",
    headers: {
      authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => {
      const user = getUser();
      setCocktail({...cocktail, favouritedBy: [...cocktail.favouritedBy, {id: user.id,
        username: user.username,
        displayName: user.displayName,} ]
      });
    })
    .catch((error) => console.log(error));
};

export const getAllBars = (setBars) => {

    return fetch("http://localhost:5000/bars/", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
    .then((res) => res.json())
    .then((res) => setBars(res))
    .catch((err) => console.log(err));
  };


  export const getBarInfo = (setBar, id) => {

    return fetch(`http://localhost:5000/bars/${id}`, {
      method: "GET",
      headers: {
      },
    })
      .then((res) => res.json())
      .then((response) => {
        const cover = response.images.filter((image) => image.isCover === 1)[0].url
        setBar({...response, cover})
      })
      .catch((error) => console.log(error));
  };

  export const getBarReviews = (setReviews, id) => {
    
    return fetch(`http://localhost:5000/bars/${id}/reviews`, {
    method: "GET",
    headers: {
      // authorization: `Bearer ${getToken()}`,
    },
  })
    .then((r) => r.json())
    .then(setReviews)
    .catch(console.error);  
  }

  export const postBarReview = (id, review) => {
    return fetch(`http://localhost:5000/bars/${id}/reviews`, {
      method: "POST",
      body: JSON.stringify(review),
      headers: {
        authorization: `Bearer ${getToken()}`,
        "content-type": "application/json",
      },
    })
      .then((r) => r.json())
      .catch(console.error);
  };

  export const updateUser = ( displayName, setDisplayName ) => {

    return fetch(`http://localhost:5000/users`, {
      method: "PUT",
      headers: {
        "authorization": `Bearer ${getToken()}`,
        "content-type": "application/json"
      },
      body: JSON.stringify({displayName})
    })
      .then((response) => response.json())
      .then((response) => setDisplayName(response.displayName))
      .catch((err) => console.log(err));
  }

  export const getUserById = (id, setUser, setLoad) => {
    
    return fetch(`http://localhost:5000/users/${id}`, {
      method: "GET",
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((response) => {
        setUser(response)
        setLoad(true)
      })
      .catch((error) => console.log(error));  
  }

  export const updateBar = ( id, bar ) => {

    return fetch(`http://localhost:5000/bars/${id}`, {
      method: "PUT",
      headers: {
        "authorization": `Bearer ${getToken()}`,
        "content-type": "application/json"
      },
      body: JSON.stringify(bar)
    })
      .then((response) => response.json())
      .catch((err) => console.log(err));
  }

  export const updateCocktail = ( id, cocktail ) => {

    return fetch(`http://localhost:5000/cocktails/${id}`, {
      method: "PUT",
      headers: {
        "authorization": `Bearer ${getToken()}`,
      },
      body: cocktail,
    })
      .then((response) => response.json())
      .catch((err) => console.log(err));
  }

  export const addCocktailToBar = ( barId, cocktailId ) => {

    return fetch(`http://localhost:5000/bars/${barId}/cocktails/${cocktailId}`, {
      method: "PUT",
      headers: {
        "authorization": `Bearer ${getToken()}`,
      }
    }).catch((err) => console.log(err));
  }

  export const removeCocktailFromBar = ( barId, cocktailId ) => {

    return fetch(`http://localhost:5000/bars/${barId}/cocktails/${cocktailId}`, {
      method: "DELETE",
      headers: {
        "authorization": `Bearer ${getToken()}`,
        "content-type": "application/json"
      },
    }).catch((err) => console.log(err));
  }

  export const deleteIngredient = ( id ) => {

    return fetch(`http://localhost:5000/cocktails/${id}`, {
      method: "DELETE",
      headers: {
        "authorization": `Bearer ${getToken()}`,
        "content-type": "application/json"
      },
    })
      .then((response) => response.json())
      .catch((err) => console.log(err));
  }

  export const removeCocktailIngredient = ( id, ingredients ) => {

      return fetch(`http://localhost:5000/cocktails/${id}/ingredients`, {
      method: "DELETE",
      body: JSON.stringify({ingredients: ingredients}),
      headers: {
        "authorization": `Bearer ${getToken()}`,
        "content-type": "application/json"
      },
    })
      .then((response) => response.json())
      .catch((err) => console.log(err));
  }

  export const addCocktailIngredient = ( id, ingredients ) => {

    return fetch(`http://localhost:5000/cocktails/${id}/ingredients`, {
      method: "PUT",
      body: JSON.stringify({ingredients}),
      headers: {
        "authorization": `Bearer ${getToken()}`,
        "content-type": "application/json"
      },
    })
      .then((response) => response.json())
      .catch((err) => console.log(err));
  }

  export const getCocktailDetails = ( id, setCocktail, setShowCocktails) => {

    return fetch("http://localhost:5000/cocktails/", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setCocktail(res);
        setShowCocktails(res);
      })
      .catch(({ message }) => {
        console.log(message);
      });
  }

  export const getAllIngredients = (setIngredients) => {

    return fetch("http://localhost:5000/ingredients", {
      method: "GET",
      headers: {
        authorization: `Bearer ${getToken()}`,
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setIngredients(res);
      })
      .catch(({ message }) => {
        console.log(message);
      })
    }

  export const getAllCocktails = (setCocktails) => {

    return fetch("http://localhost:5000/cocktails/", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.message) {
          throw new Error(res.message);
        }
        setCocktails(res);
      })
      .catch(({ message }) => {
        console.log(message);
      });
  }

  export const uploadImageToBar = (bar, event) => {
    const formData = new FormData(event.target);

    return fetch(`http://localhost:5000/bars/${bar.id}/images`, {
      method: "POST",
      body: formData,
      headers: {
        authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .catch(console.error);
  };

  export const setBarCover = (barId, imageId) => {

    return fetch(`http://localhost:5000/bars/${barId}/images/${imageId}/cover`, {
      method: "PUT",
      headers: {
        "authorization": `Bearer ${getToken()}`,
      },
    }).catch((error) => console.log(error));
  }
