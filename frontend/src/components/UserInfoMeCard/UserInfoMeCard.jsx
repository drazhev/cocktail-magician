import React, { useContext, useState } from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import LocalBar from '@mui/icons-material/LocalBar';
import { useParams } from 'react-router';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import AuthContext from '../../auth/AuthContext';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import { updateUser } from '../../services/requests';
import FavoriteCocktails from '../FavoriteCocktails/FavoriteCocktails';
import { useNavigate } from 'react-router';
import PropTypes from "prop-types";

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

const UserInfoCard = ({user, isLoaded}) => {
  const [expanded, setExpanded] = useState(false);
  const { setAuth, isLoggedIn } = useContext(AuthContext);
  const [displayName, setDisplayName] = useState('');
  const handleOpen = () => setOpen(true);
  const [open, setOpen] = useState(false);
  const history = useNavigate();

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const updateDisplayName = () => {
    updateUser(displayName, setDisplayName).then((result) => {
      setAuth({
        user: {},
        isLoggedIn: false,
      });
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      history('./login');
    });
  };

  return (
    <div style={{ marginLeft: '200px', paddingBottom: '150px', paddingTop:'40px' }}>
      <Card sx={{ width: '80%', textAlign: 'center' }}>
        <CardHeader
          style={{ backgroundColor: '#9d8a88' }}
          title={`Username: ${user.username}`}
          subheader={`Role: ${user.role}`}
        />
        <CardContent>
          <Typography variant="body2" color="text.secondary">
            Choose Display Name:
          </Typography>
          <Box
            component="form"
            sx={{
              '& > :not(style)': { m: 1, width: '25ch' },
            }}
            noValidate
            autoComplete="off"
          >
            <TextField
              id="standard-basic"
              label={user.displayName ? user.displayName : 'Display Name'}
              variant="standard"
              value={displayName}
              onChange={(event) => setDisplayName(event.target.value)}
            />
            <Stack spacing={2} direction="row" style={{ marginLeft: '385px' }}>
              <Button
                variant="outlined"
                style={{ color: 'black', borderColor: 'black', width: 'auto' }}
                onClick={updateDisplayName}
              >
                Update Display Name
              </Button>
            </Stack>
          </Box>
        </CardContent>
        <CardActions
          style={{ backgroundColor: '#9d8a88' }}
          disableSpacing
        >
          Favourite Cocktails: {isLoaded && user.favouriteCocktails.length}
          <IconButton aria-label="share" onClick={handleOpen}>
            <LocalBar />
            <FavoriteCocktails open={open} setOpen={setOpen} user={user} />
          </IconButton>
          <ExpandMore
            expand={expanded}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </ExpandMore>
          Reviews: {user.reviewedBars.length}
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Reviews:</Typography>
            {user.reviewedBars.length === 0 && (
              <span style={{ marginRight: 200 }}>No reviews</span>
            )}
            {user.reviewedBars.length !== 0 && (
              <TableContainer>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Bar Name</TableCell>
                      <TableCell align="right">Content</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {isLoaded && user.reviewedBars?.map((bar) => (
                      <TableRow
                        key={bar.id}
                        sx={{
                          '&:last-child td, &:last-child th': { border: 0 },
                        }}
                      >
                        <TableCell component="th" scope="row">
                          <Button
                            onClick={() => history(`/bars/${bar.id}`)}
                          >
                            {bar.name}
                          </Button>
                        </TableCell>
                        <TableCell align="right">{bar.text}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            )}
          </CardContent>
        </Collapse>
      </Card>
    </div>
  );
};

UserInfoCard.propTypes = {
  user: PropTypes.object.isRequired,
  isLoaded: PropTypes.bool.isRequired,
};

export default UserInfoCard;


