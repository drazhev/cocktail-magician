import React, { useContext } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Link, useNavigate } from 'react-router-dom';
import AuthContext from '../../auth/AuthContext';
import logo from '../../assets/logo8.PNG';
import LogoutIcon from '@mui/icons-material/Logout';
import { IconButton } from '@mui/material';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';

const NavBar = () => {
  const { isLoggedIn, user, setAuth } = useContext(AuthContext);
  const history = useNavigate();

  const logout = () => {
    setAuth({
      isLoggedIn: false,
      user: null,
    });
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    history('/');
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar
        sx={{
          flexGrow: 1,
          position: 'absolute',
          top: 0,
          bgcolor: 'transparent',
          boxShadow: 'none',
        }}
      >
        <Toolbar>
          <Typography
            variant="h6"
            component="div"
            sx={{
              flexGrow: 1,
              marginTop: 1,
            }}
          >
            <Link to="/">
              <img alt="logo" src={logo} height="80px" />
            </Link>
          </Typography>
          <Button
            className='navButton'
            onClick={() => history('bars')}
            style={{ fontFamily: 'Apple Color Emoji', fontSize: 'large' }}
            color="inherit"
          >
            Bars
          </Button>
          <Button
            className='navButton'
            onClick={() => history('cocktails')}
            style={{ fontFamily: 'Apple Color Emoji', fontSize: 'large' }}
            color="inherit"
          >
            Medications
          </Button>
          {!isLoggedIn ? (
            <>
              <Button
                className='navButton'
                onClick={() => history('login')}
                style={{ fontFamily: 'Apple Color Emoji', fontSize: 'large' }}
                color="inherit"
              >
                Login
              </Button>
              <Button
                className='navButton'
                onClick={() => history('register')}
                style={{ fontFamily: 'Apple Color Emoji', fontSize: 'large' }}
                color="inherit"
              >
                Register
              </Button>
            </>
          ) : (
            <>
              {user?.role === 'magician' && (
                <Button
                  className='navButton'
                  onClick={() => history('admin')}
                  style={{ fontFamily: 'Apple Color Emoji', fontSize: 'large' }}
                  color="inherit"
                >
                  Admin
                </Button>
              )}
              <IconButton
                className='navButton'
                onClick={() => history('profile')}
                style={{ fontFamily: 'Apple Color Emoji', fontSize: 'large' }}
                color="inherit"
              >{user.username}
                <AccountCircleIcon />
              </IconButton>
              <IconButton onClick={logout} color="inherit">
                <LogoutIcon />
              </IconButton>
            </>
          )}
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default NavBar;
