/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import AddIcon from '@mui/icons-material/Add';
import { addCocktailToBar, getAllCocktails } from '../../services/requests';
import { IconButton } from '@mui/material';
import { useParams } from 'react-router';

// eslint-disable-next-line react/prop-types
const AddCocktailToBar = ({ openAdd, setOpenAdd, bar, setBar }) => {
  const [cocktails, setCocktails] = useState([])
  const { id } = useParams();

  const handleClose = () => {
    setOpenAdd(false);
  };

  useEffect(() => {
    getAllCocktails(setCocktails)
  }, [id]);

  const addCocktail = (barId, cocktailId) => {
    addCocktailToBar(barId, cocktailId)
    .then(() => {
      const cocktail = cocktails.find((cocktail) => cocktail.id === cocktailId)
      setBar({...bar, cocktails: [...bar.cocktails, cocktail] })
      setOpenAdd(false)
    })
  }

  return (
    <div>
      <br />
      <Dialog onClose={handleClose} open={openAdd}>
        <DialogTitle>Add Cocktail</DialogTitle>
        <List sx={{ pt: 0 }}>
          {cocktails.filter((cocktail) => !(bar.cocktails.some((e) => e.id === cocktail.id))).map((cocktail) => (
            <ListItem key={cocktail.id}>
              <IconButton button onClick={() => addCocktail(bar.id, cocktail.id)}>
                <AddIcon />
              </IconButton>{cocktail.name}
            </ListItem>
          ))}
        </List>
      </Dialog>
    </div>
  );
};
export default AddCocktailToBar;
