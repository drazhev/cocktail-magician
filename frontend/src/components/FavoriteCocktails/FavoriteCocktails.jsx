import React, { useContext } from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import { Button } from '@mui/material';
import { useNavigate } from 'react-router';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

// eslint-disable-next-line react/prop-types
const FavoriteCocktails = ({ open, setOpen, user }) => {
  const handleClose = () => setOpen(false);
  const history = useNavigate();

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        align="center"
      >
        <Box sx={style}>
          <Typography paragraph>Favourite Cocktails:</Typography>
          {user.favouriteCocktails?.length === 0 && (
            <span style={{ marginRight: 200 }}>
              No favourite cocktails added
            </span>
          )}
          {user.favouriteCocktails?.length > 0 && (
            <TableContainer>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Cocktail ID</TableCell>
                    <TableCell align="right">Cocktail</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {user.favouriteCocktails?.map((item) => (
                    <TableRow
                      key={item.id}
                      sx={{
                        '&:last-child td, &:last-child th': { border: 0 },
                      }}
                    >
                      <TableCell component="th" scope="row">
                        <Button
                          onClick={() => history(`/cocktails/${item.id}`)}
                        >
                          {item.id}
                        </Button>
                      </TableCell>
                      <TableCell align="right">
                        <Button
                          onClick={() => history(`/cocktails/${item.id}`)}
                        >
                          {item.name}
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          )}
        </Box>
      </Modal>
    </div>
  );
};

FavoriteCocktails.propTypes = {
  open: PropTypes.bool.isRequired,
  setOpen: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
};
export default FavoriteCocktails;
