import React, { useContext, useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import AuthContext from '../../auth/AuthContext';
import { getBarInfo, updateBar } from '../../services/requests';
import { useParams } from 'react-router';
import PropTypes from 'prop-types';

// eslint-disable-next-line react/prop-types
const EditBar = ({ openEdit, setOpenEdit, bar, setBar }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const { user } = useContext(AuthContext);
  const { id } = useParams();
  const [cloneBar, setCloneBar] = useState({...bar});

  const handleClickEvent = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setOpenEdit(false);
  };

  const editTheBar = () => {
    if (cloneBar.name === '' || cloneBar.phone === '' || cloneBar.address === '') {
      return alert('Name. Phone and Address should not be empty');
    }
    if (cloneBar.name === bar.name ? delete cloneBar.name : cloneBar.name)
    updateBar(id, cloneBar)
    .then((response) => {
      setBar({...bar, ...response})
    });
  };

  return (
    <div>
      <Dialog open={openEdit} onClose={handleClose}>
        <DialogTitle>Edit Bar Info</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Hello, {user.displayName ? user.displayName : user.username}.<br />
            You can update the additional bar info:
            <br /> Name, Phone and Address
          </DialogContentText>
          <TextField
            onChange={(event) => setCloneBar({...cloneBar, name:event.target.value})}
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            value={cloneBar.name}
            fullWidth
            variant="standard"
          />
          <TextField
            onChange={(event) => setCloneBar({...cloneBar, phone:event.target.value})}
            autoFocus
            margin="dense"
            id="phone"
            label="Phone"
            value={cloneBar.phone}
            fullWidth
            variant="standard"
          />
          <TextField
            onChange={(event) => setCloneBar({...cloneBar, address:event.target.value})}
            autoFocus
            margin="dense"
            id="address"
            label="Address"
            value={cloneBar.address}
            fullWidth
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button
            onClick={(event) => {
              handleClickEvent(event);
              editTheBar();
              handleClose();
            }}
          >
            Edit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

EditBar.propTypes = {
  bar: PropTypes.object.isRequired,
};

export default EditBar;
