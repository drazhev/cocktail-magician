/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Dialog from '@mui/material/Dialog';
import AddIcon from '@mui/icons-material/Add';
import { addCocktailIngredient } from '../../services/requests';
import { ListItemIcon } from '@mui/material';
import ListItemButton from '@mui/material/ListItemButton';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';

// eslint-disable-next-line react/prop-types
const AddIngredient = ({
  openAdd,
  setOpenAdd,
  ingredients,
  onClose,
  cocktail,
  setCocktail
}) => {
  const [checked, setChecked] = useState([]);

  const handleToggle = (value) => () => {
    const index = checked.indexOf(value);
    const newChecked = [...checked];

    if (index === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(index, 1);
    }

    setChecked(newChecked);
  };

  const handleClose = () => {
    setOpenAdd(false);
  };

  const addIngredient = () => {
    if (checked.length === 0) return;

    addCocktailIngredient(cocktail.id, checked)
    .then((response) => {
      setCocktail({...cocktail, ...response})
      setOpenAdd(false)
    })
  }

  return (
    <Dialog onClose={handleClose} open={openAdd}>
      <List sx={{ width:'180px', maxWidth: 360, bgcolor: 'background.paper' }}>
        {ingredients.filter(({name}) => !cocktail.ingredients.some((ingredient) => ingredient.name === name))
        .map((item) => {
          const labelId = `checkbox-list-label-${item.id}`;

          return (
            <ListItem
              key={item.id}
              disablePadding
            >
              <ListItemButton
                role={undefined}
                onClick={handleToggle(item.name)}
                dense
              >
                <ListItemIcon>
                  <Checkbox
                    edge="start"
                    checked={checked.indexOf(item.name) !== -1}
                    tabIndex={-1}
                    disableRipple
                    inputProps={{ 'aria-labelledby': labelId }}
                  />
                </ListItemIcon>
                <ListItemText id={labelId} primary={`${item.name}`} />
              </ListItemButton>
            </ListItem>
          );
        })}
      </List>
      <IconButton onClick={() => addIngredient()}> 
        <AddIcon />Add
      </IconButton>
    </Dialog>
  );
};
export default AddIngredient;
