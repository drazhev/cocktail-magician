/* eslint-disable react/prop-types */
import React, { useContext } from 'react';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import AddIcon from '@mui/icons-material/Add';
import { useParams } from 'react-router';
import AuthContext from '../../auth/AuthContext';
import { IconButton } from '@mui/material';
import { setBarCover, uploadImageToBar } from '../../services/requests';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '1200px',
  bgcolor: '#9d8a88',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

// eslint-disable-next-line react/prop-types
const BarGallery = ({ openGallery, setOpenGallery, bar, setBar, update, setUpdate}) => {
  const handleCloseGallery = () => setOpenGallery(false);
  const {user} = useContext(AuthContext);

  const addImageToGallery = (event) => {
      uploadImageToBar(bar, event)
      .then((response) => setBar({...bar, images: response}));
  }

  const setTheCover = (imageId) => {
      setBarCover(bar.id, imageId)
      .then((response) => {
            setUpdate(!update)
        })
  }

  return (
    <div>
      <Modal
        open={openGallery}
        onClose={handleCloseGallery}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" sx={{fontFamily:'Apple Color Emoji', fontSize:'xx-large', textAlign:'center'}} variant="h6" component="h2">
            Bar Gallery
          </Typography>
          {user && user.role === 'magician' &&
            <div style={{textAlign:'center'}}>
              <form
                onSubmit={(event) => {
                event.preventDefault();
                addImageToGallery(event);
                }}
              >
                <input type="file" name="image" />
                <IconButton type='submit'>
                  <AddIcon />
                </IconButton>Add Image
              </form>
            </div>}
          <ImageList sx={{ width: 1200, height: 550 }} cols={2} rowHeight={350}>
            {bar.images?.length > 0 ? bar.images.map((image) => (
              <ImageListItem key={image.url}>
                <img
                  src={`http://localhost:5000${image.url}`}
                  srcSet={`http://localhost:5000${image.url}`}
                  alt={image.id}
                  loading="lazy"
                  style={{height:'100px'}}
                />
                {user && user.role === 'magician' ? 
                  image.isCover === 0 ?
                    <Button onClick={() => setTheCover(image.id)} variant='text' sx={{color:'black', borderColor:'black', fontFamily:'emoji'}}>
                      Set as cover photo
                    </Button>
                  :
                    <Button variant="text" sx={{color:'#b32727', borderColor:'black', textAlign:'center', fontFamily:'emoji'}}>
                      Current Cover
                    </Button> : null}
              </ImageListItem>
            ))
            :
            null}
          </ImageList>
        </Box>
      </Modal>
    </div>
  );
}

export default BarGallery;
