import React, { useContext, useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import AuthContext from '../../auth/AuthContext';
import { updateCocktail } from '../../services/requests';
import { useParams } from 'react-router';
import PropTypes from 'prop-types';

// eslint-disable-next-line react/prop-types
const EditCocktail = ({ openEdit, setOpenEdit, cocktail, setCocktail }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const { user } = useContext(AuthContext);
  const { id } = useParams();
  const [cloneCocktail, setCloneCocktail] = useState({...cocktail});

  const handleClickEvent = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setOpenEdit(false);
  };

  const editTheCocktail = () => {
    const formData = new FormData();

    formData.append('name', cloneCocktail.name)
    formData.append('image', cloneCocktail.image)

    if (cloneCocktail.name === '') {
      return alert('Name should not be empty');
    }

    if (cloneCocktail.name === cocktail.name) {
      formData.delete('name')
    }

    updateCocktail(id, formData)
    .then((response) => {
      setCocktail({...cocktail, ...response})
    });
  };

  return (
    <div>
      <Dialog open={openEdit} onClose={handleClose}>
        <DialogTitle>Edit Cocktail Name</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Hello, {user.displayName ? user.displayName : user.username}.<br />
            You can update the name of the cocktail:
          </DialogContentText>
          <TextField
            onChange={(event) => setCloneCocktail({...cloneCocktail, name:event.target.value})}
            autoFocus
            margin="dense"
            id="name"
            label="Name"
            value={cloneCocktail.name}
            fullWidth
            variant="standard"
          />
          <DialogContentText>
            You can update the picture of the cocktail:
          </DialogContentText>
          <input 
            type="file" onChange={(event) => {
            setCloneCocktail({...cloneCocktail, image:event.target.files[0]})
          }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button
            onClick={(event) => {
              handleClickEvent(event);
              editTheCocktail();
              handleClose();
            }}
          >
            Edit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

EditCocktail.propTypes = {
  cocktail: PropTypes.object.isRequired,
};

export default EditCocktail;
