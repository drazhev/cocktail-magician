import React, { useContext, useEffect, useState } from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import LocalBar from '@mui/icons-material/LocalBar';
import { useParams } from 'react-router';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import AuthContext from '../../auth/AuthContext';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import FavoriteCocktails from '../FavoriteCocktails/FavoriteCocktails';
import { useNavigate } from 'react-router';
import { getUserById } from '../../services/requests';

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

const UserProfileCard = () => {
  const { id } = useParams();
  const [expanded, setExpanded] = useState(false);
  const { setAuth, isLoggedIn } = useContext(AuthContext);
  const [displayName, setDisplayName] = useState('');
  const handleOpen = () => setOpen(true);
  const [open, setOpen] = useState(false);
  const history = useNavigate();
  const [user, setUser] = useState([]);
  const [load, setLoad] = useState();

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  useEffect(() => {
    getUserById( id, setUser, setLoad );
  }, [id])

  return (
    <div style={{ marginLeft:'200px', paddingBottom: '200px', paddingTop:'110px' }}>
      <Card sx={{ width: '80%', textAlign: 'center' }}>
        <CardHeader
          style={{ backgroundColor: '#9d8a88', height:"30px" }}
          title={`Username: ${user.username}`}
          subheader={`Role: ${user.role}`}
        />
        <CardContent>
          <Typography>
            {user.displayName ? `Display Name: ${user.displayName}` : `Username: ${user.username}`}
          </Typography>
        </CardContent>
        <CardActions
          style={{ backgroundColor: '#9d8a88' }}
          disableSpacing
        >
          Favourite Cocktails: {load && user.favouriteCocktails.length}
          <IconButton aria-label="share" onClick={handleOpen}>
            <LocalBar />
            <FavoriteCocktails open={open} setOpen={setOpen} user={user} />
          </IconButton>
          <ExpandMore
            expand={expanded}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </ExpandMore>
          Reviews: {load && user.reviewedBars.length}
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Reviews:</Typography>
            {load && user.reviewedBars.length === 0 && (
              <span style={{ marginRight: 200 }}>No reviews</span>
            )}
            {load && user.reviewedBars.length !== 0 && (
              <TableContainer>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Bar Name</TableCell>
                      <TableCell align="right">Content</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {load && user.reviewedBars.map((bar) => (
                      <TableRow
                        key={bar.id}
                        sx={{
                          '&:last-child td, &:last-child th': { border: 0 },
                        }}
                      >
                        <TableCell component="th" scope="row">
                          <Button onClick={() => history(`/bars/${bar.id}`)}>
                            {bar.name}
                          </Button>
                        </TableCell>
                        <TableCell align="right">{bar.text}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            )}
          </CardContent>
        </Collapse>
      </Card>
    </div>
  );
};

export default UserProfileCard;
