import * as React from 'react';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import logo from '../../assets/logo8.PNG';
import { AppBar, Toolbar } from '@mui/material';
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router';

const AppFooter = () => {
  const history = useNavigate();

  return (
    <AppBar  
      color="primary"
      position="static"
      sx={{
        mt: 'auto',
        bgcolor: '#0f1112',
        textAlign: 'center',
        bottom: '0',
        width: '100%',
        height: '100%',
      }}
    >
      <Container maxWidth="md">
        <Toolbar 
          sx={{
            justifyContent: "space-between"
          }}
        >
          <Button onClick={() => history("about")} style={{ color: 'rgb(237 236 239 / 83%)', fontFamily: 'Apple Color Emoji', fontSize: 'large' }}>
            Meet the team
          </Button>
          <Typography variant="body1" color="inherit">
            <img alt="logo" src={logo} width="200px" />
          </Typography>
          <Typography style={{ color: 'rgb(237 236 239 / 83%)', fontFamily: 'emoji', fontSize: 'large' }}>
            © 2021 Ivo & Alex
          </Typography>
        </Toolbar>
      </Container>
    </AppBar>
  );
};

export default AppFooter;
